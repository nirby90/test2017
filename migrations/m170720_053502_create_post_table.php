<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m170720_053502_create_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('post', [
           
			'id' => 'pk',
			'title' => 'string',
			'body' => 'text',
			'category' => 'string',
			'author' => 'string',
			'status' => 'string',
			'created_at' => 'integer',
			'updated_at' => 'integer',
			'created_by'=>'integer',
		    'updated_by'=>'integer',	
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('post');
    }
}
